package main

import (
	"strconv"
)

func main() {
	println(letterCombinations("23"))
}

func letterCombinations(digits string) []string {
	if len(digits) == 0 {
		return nil
	}

	var ans []string
	nextAppend("", digits, &ans)
	return ans
}

var letters = [...]string{"", "", "abc", "def", "ghi", "jkl",
	"mno", "pqrs", "tuv", "wxyz"}

func nextAppend(prefix string, remainDigits string, ans *[]string) {
	if len(remainDigits) == 0 {
		*ans = append(*ans, prefix)
		return
	}
	println("remainsDigits:", remainDigits)
	println("remainsDigits - 0: ", string(remainDigits[0]))
	v, _ := strconv.Atoi(string(remainDigits[0]))
	println("v: ", v)
	for i := 0; i < len(letters[v]); i++ {
		nextAppend(prefix+string(letters[v][i]), remainDigits[1:], ans)
	}
}

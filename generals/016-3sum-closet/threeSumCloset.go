package main

import (
	"math"
	"sort"
)

func main() {

}

func threeSumClosest(nums []int, target int) int {
	closestSum := 3000
	closestDiff := 3000
	sum := 0
	diff := 0
	sort.Ints(nums)

	for i := 0; i < len(nums)-2; i++ {
		j := i + 1
		k := len(nums) - 1
		for j < k {
			sum = nums[i] + nums[j] + nums[k]
			diff = int(math.Abs(float64(sum - target)))
			if diff < closestDiff {
				closestSum = sum
				closestDiff = diff
			}
			if sum == target {
				break
			} else if sum > target {
				k--
			} else {
				j++
			}
		}
	}
	return closestSum
}

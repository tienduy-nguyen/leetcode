package leetcode

import (
	"sort"
)

func threeSum(nums []int) [][]int {
	res := [][]int{}
	counter := map[int]int{}
	for _, value := range nums {
		counter[value]++
	}
	uniqNums := []int{}
	for key := range counter {
		uniqNums = append(uniqNums, key)
	}
	sort.Ints(uniqNums)

	c := 0

	for i:=0; i<len(uniqNums); i++{
		if uniqNums[i] == 0 && counter[0] >=3 {
			res = append(res, []int{0,0,0})
		}
		for j:= i+1; j<len(uniqNums); j++{
			if(uniqNums[i]*2 + uniqNums[j] == 0) && counter[uniqNums[i]]>1{
				res = append(res, []int{uniqNums[i], uniqNums[i], uniqNums[j]})

			}
			if(uniqNums[i] + uniqNums[j]*2 == 0) && counter[uniqNums[j]]>1{
				res = append(res, []int {uniqNums[i], uniqNums[j], uniqNums[j]})
			
			}
			c = 0 - uniqNums[i] - uniqNums[j]

			if c>uniqNums[j] && counter[c] > 0{
				res = append(res,  []int{uniqNums[i], uniqNums[j],c})
			}
		}
	}
	return res

}


// sort case insensitive
// sort.Slice(data, func(i, j int) bool 
{ return strings.ToLower(data[i]) < strings.ToLower(data[j]) })